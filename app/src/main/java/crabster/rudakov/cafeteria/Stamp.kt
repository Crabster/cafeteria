package crabster.rudakov.cafeteria

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View

class Stamp(context: Context, attrs: AttributeSet) : View(context, attrs) {

    private val text = context.getString(R.string.developer_name)
    private val path = Path()
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        strokeWidth = 5f
        textSize = 35f
        style = Paint.Style.FILL
        color = Color.WHITE
    }

    /**
     * This method draws developer's stamp.
     */
    override fun onDraw(canvas: Canvas?) {
        path.addCircle(125f, 130f, 70f, Path.Direction.CW)
        canvas?.drawTextOnPath(text, path, 160f, -10f, paint)
        paint.style = Paint.Style.STROKE
        canvas?.drawPath(path, paint)
    }

}