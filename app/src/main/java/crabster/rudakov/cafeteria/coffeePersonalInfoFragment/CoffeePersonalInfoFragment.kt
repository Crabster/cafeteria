package crabster.rudakov.cafeteria.coffeePersonalInfoFragment

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.setFragmentResult
import crabster.rudakov.cafeteria.R
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee
import crabster.rudakov.cafeteria.databinding.FragmentCoffeePersonalInfoBinding
import crabster.rudakov.cafeteria.helpers.Constants
import crabster.rudakov.cafeteria.helpers.MultimediaEffects
import crabster.rudakov.cafeteria.toppingFragment.ToppingFragment

class CoffeePersonalInfoFragment: Fragment() {

    private lateinit var coffee: Coffee

    private var _binding: FragmentCoffeePersonalInfoBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        _binding = FragmentCoffeePersonalInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        coffee = requireArguments().getParcelable(Constants.COFFEE_OBJECT_KEY)!!

        binding.flagInfoView.setImageResource(coffee.flagId)
        binding.flagInfoView.setOnClickListener { v -> initGoogleActivity(getUri(v)) }
        binding.descriptionTextView.setOnClickListener { v -> initGoogleActivity(getUri(v)) }
        binding.descriptionTextView.setText(coffee.descriptionId)
        binding.yeahButton.setOnClickListener { createToppingFragment() }

        initAnimation()
    }

    /**
     * This method creates next fragment & saves instances of
     * chosen coffee's parameters.
     */
    private fun createToppingFragment() {
        setFragmentResult(Constants.FRAGMENT_RESULT_KEY, bundleOf(Constants.COFFEE_OBJECT_KEY to coffee))
        val transaction: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_coffee_personal_info_container, ToppingFragment())
            .addToBackStack(null).commit()
        MultimediaEffects.playSound(this.requireContext(), R.raw.big_buttons)
    }

    /**
     * This method composes URI depending on the received View.
     *
     * @param view element of the screen
     * @return resource identifier
     */
    private fun getUri(view: View): Uri {
        val countryName: String = coffee.countryOfOriginId!!
        lateinit var uriString: String
        when (view.id) {
            R.id.flag_info_view -> uriString = Constants.URI_GOOGLE_MAPS + countryName
            R.id.description_text_view -> {
                val coffeeSort: String = getString(coffee.sortId)
                uriString = (Constants.URI_GOOGLE_SEARCH
                        + countryName + " "
                        + coffeeSort + " "
                        + getString(Constants.COFFEE_SEARCH_ADDITIONAL_KEY))
            }
        }
        return Uri.parse(uriString)
    }

    /**
     * This method initializes Activity of Google services
     * depending on the received URI.
     *
     * @param uri resource identifier
     */
    private fun initGoogleActivity(uri: Uri) {
        val intentGoogle = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intentGoogle)
    }

    /**
     * This method initializes animation of Views.
     */
    private fun initAnimation() {
        MultimediaEffects.createAnimation(this.requireContext(), binding.infoView, R.anim.alpha_transparency)
        MultimediaEffects.createAnimation(this.requireContext(), binding.flagInfoView, R.anim.scale_expand_from_center)
        MultimediaEffects.createAnimation(this.requireContext(), binding.descriptionTextView, R.anim.rotate)
        MultimediaEffects.createAnimation(this.requireContext(), binding.googleMapsNoticeView, R.anim.rotate)
    }

}