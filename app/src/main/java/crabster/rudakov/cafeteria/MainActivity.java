package crabster.rudakov.cafeteria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.CursorWrapper;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import crabster.rudakov.cafeteria.coffeeActivity.CoffeeActivity;
import crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase.CoffeeDbManager;
import crabster.rudakov.cafeteria.databinding.ActivityMainBinding;
import crabster.rudakov.cafeteria.helpers.DbaseFiller;
import crabster.rudakov.cafeteria.helpers.MultimediaEffects;
import crabster.rudakov.cafeteria.helpers.InformationDialogFragment;


/**
 * @author Alexey Rudakov <https://gitlab.com/Crabster>
 * Project "Cafeteria"
 */

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        initAnimation();

        binding.goButton.setOnClickListener(v -> {
            showProgressDialog();
            new Thread(this::verifyFillingDatabase).start();
            initCoffeeActivity();
        });
    }

    /**
     * The method was overridden for destroying ProgressDialog, that performed
     * its function, cause the database has already been filled.
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        progressDialog.dismiss();
    }

    /**
     * The method creates the menu, inflating menu from the resource file.
     *
     * @param menu the menu item
     * @return true for the menu to be displayed, false it will not be shown
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * The method processes click on the menu's icon and calls dialog window.
     *
     * @param item of the menu
     * @return false to allow normal menu processing to proceed,
     * true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.information) {
            InformationDialogFragment dialog = new InformationDialogFragment();
            dialog.show(getSupportFragmentManager(), getString(R.string.information_title_dialog_window));
            MultimediaEffects.Companion.playSound(getApplicationContext(), R.raw.check_topping);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * The method creates ProgressDialog to show processes of filling the database.
     */
    private void showProgressDialog() {
        progressDialog = new ProgressDialog(MainActivity.this, ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage(getString(R.string.database_filling_indication_dialog_window));
        progressDialog.show();
    }

    /**
     * This method verifies filling of the database and fills it in negative case.
     */
    private synchronized void verifyFillingDatabase() {
        CoffeeDbManager dbManager = new CoffeeDbManager(this);
        CursorWrapper cursorWrapper = dbManager.queryCoffees(null);
        if (cursorWrapper.getCount() == 0) {
            DbaseFiller dbaseFiller = new DbaseFiller(this);
            dbaseFiller.fillCoffeeDataBase();
        }
        cursorWrapper.close();
    }

    /**
     * This method initializes next Activity.
     */
    private void initCoffeeActivity() {
        Intent intent = new Intent(MainActivity.this, CoffeeActivity.class);
        MultimediaEffects.Companion.playSound(getApplicationContext(), R.raw.big_buttons);
        startActivity(intent);
        MultimediaEffects.Companion.displayToast(getApplicationContext(), getString(R.string.make_your_choice));
    }

    /**
     * This method initializes animation of Views.
     */
    private void initAnimation() {
        MultimediaEffects.Companion.createAnimation(this, binding.mainImageView, R.anim.alpha_transparency);
        MultimediaEffects.Companion.createAnimation(this, binding.firstGreeting, R.anim.my_combo);
        MultimediaEffects.Companion.createAnimation(this, binding.inviteToOrder, R.anim.translation_from_left_upper_corner);
    }

}