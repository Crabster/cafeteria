package crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers

import android.os.Parcelable
import androidx.annotation.StringRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class Coffee(
    @StringRes internal val flagId: Int,
    internal val countryOfOriginId: String?,
    @StringRes internal val sortId: Int,
    @StringRes internal val priceOfCupId: Int,
    @StringRes internal val descriptionId: Int) : Parcelable {

    fun getFlagId(): Int {
        return flagId
    }

    fun getCountryOfOriginId(): String? {
        return countryOfOriginId
    }

    fun getSortId(): Int {
        return sortId
    }

    fun getPriceOfCupId(): Int {
        return priceOfCupId
    }

    fun getDescriptionId(): Int {
        return descriptionId
    }

}