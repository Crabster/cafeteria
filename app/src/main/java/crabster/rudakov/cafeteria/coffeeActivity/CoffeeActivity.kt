package crabster.rudakov.cafeteria.coffeeActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import crabster.rudakov.cafeteria.R
import crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase.CoffeeDbManager
import crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase.CoffeeDbSchema
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.SimpleItemTouchHelperCallback
import crabster.rudakov.cafeteria.databinding.ActivityCoffeeBinding

class CoffeeActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var coffeeTypeAdapter: CoffeeTypeAdapter

    private lateinit var binding: ActivityCoffeeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCoffeeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        recyclerView = findViewById(R.id.single_coffee_type)
        recyclerView.layoutManager = LinearLayoutManager(this)
        displayCoffeeList(null)

        binding.headerCountryOfOrigin.setOnClickListener {
            displayCoffeeList(CoffeeDbSchema.Columns.COUNTRY_OF_ORIGIN)
        }
        binding.headerSortCoffee.setOnClickListener {
            displayCoffeeList(CoffeeDbSchema.Columns.SORT)
        }
        binding.headerPricePerCup.setOnClickListener {
            displayCoffeeList(CoffeeDbSchema.Columns.PRICE_OF_CUP)
        }
    }

    /**
     * This method realizes rendering the updated list after dragging or
     * removing list's elements from the screen.
     */
    override fun onResume() {
        super.onResume()
        val callback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(coffeeTypeAdapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(recyclerView)
    }

    /**
     * This method displays received coffee list into the RecyclerView in
     * accordance with the specified order or without it.
     *
     * @param orderBy the name of database table's column, by that the list must be sorted
     */
    private fun displayCoffeeList(orderBy: String?) {
        val manager = CoffeeDbManager(this)
        val coffees: List<Coffee> = manager.getCoffeeList(orderBy)
        coffeeTypeAdapter = CoffeeTypeAdapter(coffees, this)
        recyclerView.adapter = coffeeTypeAdapter
        coffeeTypeAdapter.stateRestorationPolicy =
            RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

}