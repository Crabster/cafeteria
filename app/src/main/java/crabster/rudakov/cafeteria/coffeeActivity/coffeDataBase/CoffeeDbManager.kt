package crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee

class CoffeeDbManager(context: Context) {

    private val coffees: MutableList<Coffee> = mutableListOf()
    private var db: SQLiteDatabase? = null

    init {
        db = CoffeeDbHelper(context).writableDatabase
    }

    fun getDb(): SQLiteDatabase? {
        return db
    }

    /**
     * This method adds data from the set of values into the database.
     * It works in multithreading mode.
     */
    @Synchronized
    fun addCoffee(coffee: Coffee?) {
        val values: ContentValues = getContentValues(coffee!!)
        db!!.insert(CoffeeDbSchema.CoffeeTable.NAME, null, values)
    }

    /**
     * This method receives a list of objects with help of special cursorWrapper,
     * which goes in order from the first row in the database to the last row.
     *
     * @param orderBy the name of database table's column, by that the list must be sorted
     * @return list of objects, recovered from the database
     */
    fun getCoffeeList(orderBy: String?): MutableList<Coffee> {
        queryCoffees(orderBy).use {
                cursorWrapper -> cursorWrapper.moveToFirst()
            while (!cursorWrapper.isAfterLast) {
                val coffee: Coffee = cursorWrapper.getCoffee()
                coffees.add(coffee)
                cursorWrapper.moveToNext()
            }
        }
        return coffees
    }

    /**
     * This method creates special object, which will be used for extracting
     * data from the database.
     *
     * @param orderBy the name of database table's column, by that the list must be sorted
     * @return special wrapper for cursor
     */
    fun queryCoffees(orderBy: String?): CoffeeCursorWrapper {
        val cursor = db!!.query(CoffeeDbSchema.CoffeeTable.NAME,
                                                   null,
                                                   null,
                                                null,
                                                    null,
                                                     null,
                                                          orderBy)
        return CoffeeCursorWrapper(cursor)
    }

    /**
     * This method creates a set of values, linking the names of columns
     * of the database table and object parameters.
     *
     * @return set of related data
     */
    companion object {
        private fun getContentValues(coffee: Coffee): ContentValues {
            val values = ContentValues()
            values.put(CoffeeDbSchema.Columns.FLAG_ID, coffee.getFlagId())
            values.put(CoffeeDbSchema.Columns.COUNTRY_OF_ORIGIN, coffee.getCountryOfOriginId())
            values.put(CoffeeDbSchema.Columns.SORT, coffee.getSortId())
            values.put(CoffeeDbSchema.Columns.PRICE_OF_CUP, coffee.getPriceOfCupId())
            values.put(CoffeeDbSchema.Columns.DESCRIPTION, coffee.getDescriptionId())
            return values
        }
    }

}