package crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers

interface ItemTouchHelperAdapter {

    /**
     * This method is called when the element has been dragged far enough to cause a move.
     * It changes position of element and notify adapter about changing.
     *
     * @param fromPosition current position number in the list
     * @param toPosition target position number in the list
     * */
    fun onItemMove(fromPosition: Int, toPosition: Int)

        /**
         * This method is called when element is already removed. It called after data correction,
         * to display the changes and notify adapter about changes.
         *
         * @param position number of the element's position
         * */
    fun onItemDismiss(position: Int)

}