package crabster.rudakov.cafeteria.coffeeActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee;
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.ItemTouchHelperAdapter;

/**
 * This class manages the process of putting elements of the list
 * into the RecyclerView.
 */
public class CoffeeTypeAdapter extends RecyclerView.Adapter<CoffeeTypeHolder> implements ItemTouchHelperAdapter {

    private final List<Coffee> coffees;
    private final Context context;

    public CoffeeTypeAdapter(List<Coffee> coffees, Context context) {
        this.coffees = coffees;
        this.context = context;
    }

    /**
     * This method is called by RecyclerView whenever it needs to create
     * a new ViewHolder. The method creates and initializes the ViewHolder
     * and its associated View.
     *
     * @param viewGroup location of the element
     * @param count index of the element on the screen
     * @return holder creating a list item, by calling RecyclerView
     */
    @NonNull
    @Override
    public CoffeeTypeHolder onCreateViewHolder(ViewGroup viewGroup, int count) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        return new CoffeeTypeHolder(inflater, viewGroup, context);
    }

    /**
     * This method is called by RecyclerView to associate a ViewHolder with
     * data. The method fetches the appropriate data and uses the data to
     * fill in the view holder's layout.
     *
     * @param holder holder of the RecyclerView
     * @param position number of the element's position
     */
    @Override
    public void onBindViewHolder(CoffeeTypeHolder holder, int position) {
        Coffee coffee = coffees.get(position);
        holder.bind(coffee);
    }

    /**
     * This method is called by RecyclerView to get the size of the data list.
     *
     * @return quantity of elements of the RecyclerView
     */
    @Override
    public int getItemCount() {
        return coffees.size();
    }

    /**
     * This method is called when the element has been dragged far enough to cause a move.
     * It changes position of element and notify adapter about changing.
     *
     * @param fromPosition current position number in the list
     * @param toPosition target position number in the list
     */
    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Coffee previous = coffees.remove(fromPosition);
        coffees.add(toPosition > fromPosition ? toPosition - 1 : toPosition, previous);
        notifyItemMoved(fromPosition, toPosition);
    }

    /**
     * This method is called when element is already removed. It called after data correction,
     * to display the changes and notify adapter about changes.
     *
     * @param position number of the element's position
     */
    @Override
    public void onItemDismiss(int position) {
        coffees.remove(position);
        notifyItemRemoved(position);
    }

}