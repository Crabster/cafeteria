package crabster.rudakov.cafeteria.coffeeActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import crabster.rudakov.cafeteria.R;
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee;
import crabster.rudakov.cafeteria.coffeePersonalInfoFragment.CoffeePersonalInfoFragment;
import crabster.rudakov.cafeteria.helpers.MultimediaEffects;
import crabster.rudakov.cafeteria.helpers.Constants;

/**
 * This class manages the process of generation each element of the list.
 */
public class CoffeeTypeHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,
        MenuItem.OnMenuItemClickListener {

    private ImageView flagView;
    private TextView countryOfOrigin;
    private TextView sortOfCoffee;
    private TextView pricePerCup;
    private final Context context;
    private Coffee coffee;

    public CoffeeTypeHolder(LayoutInflater inflater, ViewGroup viewGroup, Context context) {
        super(inflater.inflate(R.layout.single_coffee_item, viewGroup, false));
        this.context = context;
    }

    /**
     * This method binds object's parameters to views and sets listeners on them.
     *
     * @param coffee an object from List<Coffee> coffees
     */
    public void bind(Coffee coffee) {
        flagView = itemView.findViewById(R.id.flag_view);
        flagView.setImageResource(coffee.getFlagId());
        flagView.setOnCreateContextMenuListener(this);

        countryOfOrigin = itemView.findViewById(R.id.country_of_origin);
        countryOfOrigin.setOnClickListener(v -> initCoffeePersonalInfoFragment());
        countryOfOrigin.setText(coffee.getCountryOfOriginId());

        sortOfCoffee = itemView.findViewById(R.id.sort_of_coffee);
        sortOfCoffee.setOnClickListener(v -> initCoffeePersonalInfoFragment());
        sortOfCoffee.setText(coffee.getSortId());

        pricePerCup = itemView.findViewById(R.id.price_per_cup);
        pricePerCup.setOnClickListener(v -> initCoffeePersonalInfoFragment());
        pricePerCup.setText(coffee.getPriceOfCupId() + "$");

        initAnimation();
        this.coffee = coffee;
    }

    /**
     * This method initializes CoffeePersonalInfoFragment, with personal information of every
     * each sort of coffee after click on text views of each element.
     */
    private void initCoffeePersonalInfoFragment() {
        MultimediaEffects.Companion.playSound(context, R.raw.big_buttons);
        MultimediaEffects.Companion.displayToast(context, context.getString(R.string.good_choice));
        CoffeeActivity coffeeActivity = (CoffeeActivity) context;
        FragmentManager fragmentManager = coffeeActivity.getSupportFragmentManager();
        if (fragmentManager.getFragments().isEmpty()) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.COFFEE_OBJECT_KEY, coffee);
            fragmentManager.beginTransaction()
                    .setReorderingAllowed(true)
                    .add(R.id.fragment_coffee_personal_info_container, CoffeePersonalInfoFragment.class, bundle)
                    .addToBackStack(null)
                    .commit();
        }
    }

    /**
     * This method initializes animation of Views.
     */
    private void initAnimation() {
        MultimediaEffects.Companion.createAnimation(context, flagView, R.anim.scale_expand_from_right_lower_corner);
        MultimediaEffects.Companion.createAnimation(context, countryOfOrigin, R.anim.alpha_transparency);
        MultimediaEffects.Companion.createAnimation(context, sortOfCoffee, R.anim.alpha_transparency);
        MultimediaEffects.Companion.createAnimation(context, pricePerCup, R.anim.alpha_transparency);
    }

    /**
     * This method creates context menu, that called by long click on flag image view of
     * each element and sets long click listener on it.
     *
     * @param menu     context menu
     * @param v        view for which the context menu is being built
     * @param menuInfo extra information about the item for which the context menu should be shown
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(context.getString(R.string.zoom_context_menu)).setOnMenuItemClickListener(this);
    }

    /**
     * This method realizes reaction on long click on flag image view of each element,
     * initializing any Activity, that could display zoomable national flag after.
     *
     * @param item context menu item
     * @return true to consume this click and prevent others from executing
     */
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Intent intent = new Intent(Constants.INTENT_FILTER_NAME_ZOOMABLE);
        intent.putExtra(Constants.FLAG_ID_KEY, coffee.getFlagId());
        MultimediaEffects.Companion.playSound(context, R.raw.big_buttons);
        context.startActivity(intent);
        return true;
    }

}

