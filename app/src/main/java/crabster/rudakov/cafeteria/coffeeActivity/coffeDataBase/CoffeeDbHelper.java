package crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import crabster.rudakov.cafeteria.helpers.Constants;

public class CoffeeDbHelper extends SQLiteOpenHelper {

    public CoffeeDbHelper(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.VERSION);
    }

    /**
     * This method is called when the database is created for the first time.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + CoffeeDbSchema.CoffeeTable.NAME +
                "(_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CoffeeDbSchema.Columns.FLAG_ID + " INTEGER, " +
                CoffeeDbSchema.Columns.COUNTRY_OF_ORIGIN + " INTEGER, " +
                CoffeeDbSchema.Columns.SORT + " INTEGER, " +
                CoffeeDbSchema.Columns.PRICE_OF_CUP + " INTEGER, " +
                CoffeeDbSchema.Columns.DESCRIPTION + " INTEGER)");
    }

    /**
     * This method is called when the database needs to be upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CoffeeDbSchema.CoffeeTable.NAME);
        onCreate(sqLiteDatabase);
    }

}
