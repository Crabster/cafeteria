package crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class SimpleItemTouchHelperCallback(private val adapter: ItemTouchHelperAdapter?) :
    ItemTouchHelper.Callback() {

    /**
     * This method determines the possible directions of moving a list item
     * by dragging(up - down) and swiping (left - right).
     *
     * @param recyclerView RecyclerView object
     * @param viewHolder holder of RecyclerView's element
     * @return an integer composed of the given drag and swipe flags
     */
    override fun getMovementFlags(recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder): Int {
        val dragFlags: Int = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags: Int = ItemTouchHelper.END or ItemTouchHelper.START
        return makeMovementFlags(dragFlags, swipeFlags)
    }

    /**
     * This method moves the dragged item from its old position to the new position
     * and notifies adapter about the movement of the list item.
     *
     * @param recyclerView RecyclerView object
     * @param source current position number in the list
     * @param target target position number in the list
     * @return true if the viewHolder has been moved to the adapter position of target
     */
    override fun onMove(recyclerView: RecyclerView,
        source: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder): Boolean {
        adapter!!.onItemMove(source.adapterPosition, target.adapterPosition)
        return true
    }

    /**
     * This method moves the swiped item to the specified direction
     * and notifies adapter about the swiping of the list item.
     *
     * @param viewHolder holder of RecyclerView's element
     * @param direction movement direction
     */
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        adapter!!.onItemDismiss(viewHolder.adapterPosition)
    }

    /**
     * This method specifies that all possible actions will be done
     * by long click on element of the list or not.
     *
     * @return true if actions will be done by long click
     */
    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    /**
     * This method specifies that swipe is enable to be initiated
     * at any point of the list item.
     *
     * @return true if actions will be enable at any point of element
     */
    override fun isItemViewSwipeEnabled(): Boolean {
        return true
    }

}