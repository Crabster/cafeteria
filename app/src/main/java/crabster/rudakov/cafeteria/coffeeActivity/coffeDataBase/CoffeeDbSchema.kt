package crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase

object CoffeeDbSchema {

    object CoffeeTable {
        const val NAME = "COFFEE_TABLE"
    }

    object Columns {
        const val FLAG_ID = "FLAG_ID"
        const val COUNTRY_OF_ORIGIN = "COUNTRY_OF_ORIGIN"
        const val SORT = "SORT"
        const val PRICE_OF_CUP = "PRICE_OF_CUP"
        const val DESCRIPTION = "DESCRIPTION"
    }

}