package crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase

import android.database.Cursor
import android.database.CursorWrapper
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee

class CoffeeCursorWrapper(cursor: Cursor): CursorWrapper(cursor) {

    /**
     * This method recovers an object, getting its parameters by columns of
     * each row from the database.
     *
     * @return object by class Coffee
     */
    fun getCoffee(): Coffee {
        val flagId = getInt(getColumnIndex(CoffeeDbSchema.Columns.FLAG_ID))
        val countryOfOriginId = getString(getColumnIndex(CoffeeDbSchema.Columns.COUNTRY_OF_ORIGIN))
        val sortId = getInt(getColumnIndex(CoffeeDbSchema.Columns.SORT))
        val priceOfCupId = getInt(getColumnIndex(CoffeeDbSchema.Columns.PRICE_OF_CUP))
        val descriptionId = getInt(getColumnIndex(CoffeeDbSchema.Columns.DESCRIPTION))
        return Coffee(flagId,
                      countryOfOriginId,
                      sortId,
                      priceOfCupId,
                      descriptionId)
    }

}