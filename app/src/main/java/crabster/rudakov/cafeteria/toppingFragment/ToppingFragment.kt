package crabster.rudakov.cafeteria.toppingFragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.setFragmentResultListener
import crabster.rudakov.cafeteria.R
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee
import crabster.rudakov.cafeteria.databinding.FragmentToppingBinding
import crabster.rudakov.cafeteria.emailFragment.EmailFragment
import crabster.rudakov.cafeteria.helpers.Constants
import crabster.rudakov.cafeteria.helpers.MultimediaEffects

class ToppingFragment : Fragment() {

    private var quantityOfCups = 1
    private var coffee: Coffee? = null
    private val toppingNameList = ArrayList<String>()

    private var _binding: FragmentToppingBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {

        if (savedInstanceState == null) { displayLoadedDataPreferences() }

        _binding = FragmentToppingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val groupOnClickListener =
            View.OnClickListener {
                when (it.id) {
                    R.id.toppings_whipped_cream, R.id.toppings_chocolate, R.id.toppings_nutmeg,
                    R.id.toppings_mint, R.id.toppings_ice_cream, R.id.toppings_peanut_butter,
                    R.id.toppings_marshmallow, R.id.toppings_cardamom, R.id.toppings_honey,
                    R.id.toppings_cinnamon, R.id.toppings_sprinkles, R.id.toppings_nutella,
                    R.id.toppings_lavender, R.id.toppings_turmeric,
                    R.id.toppings_sweet_cream -> MultimediaEffects.playSound(
                        this.requireContext(), R.raw.check_topping)
                    R.id.decrement_button -> decrement()
                    R.id.increment_button -> increment()
                    R.id.order_button -> if (quantityOfCups > 0) {
                        selectTopping()
                        createEmailFragment()
                    } else MultimediaEffects.displayToast(this.requireContext(), getString(R.string.minimum_size_of_order))
                }
            }
        binding.toppingsWhippedCream.setOnClickListener(groupOnClickListener)
        binding.toppingsChocolate.setOnClickListener(groupOnClickListener)
        binding.toppingsNutmeg.setOnClickListener(groupOnClickListener)
        binding.toppingsMint.setOnClickListener(groupOnClickListener)
        binding.toppingsIceCream.setOnClickListener(groupOnClickListener)
        binding.toppingsPeanutButter.setOnClickListener(groupOnClickListener)
        binding.toppingsMarshmallow.setOnClickListener(groupOnClickListener)
        binding.toppingsCardamom.setOnClickListener(groupOnClickListener)
        binding.toppingsHoney.setOnClickListener(groupOnClickListener)
        binding.toppingsCinnamon.setOnClickListener(groupOnClickListener)
        binding.toppingsSprinkles.setOnClickListener(groupOnClickListener)
        binding.toppingsNutella.setOnClickListener(groupOnClickListener)
        binding.toppingsLavender.setOnClickListener(groupOnClickListener)
        binding.toppingsTurmeric.setOnClickListener(groupOnClickListener)
        binding.toppingsSweetCream.setOnClickListener(groupOnClickListener)
        binding.decrementButton.setOnClickListener(groupOnClickListener)
        binding.incrementButton.setOnClickListener(groupOnClickListener)
        binding.orderButton.setOnClickListener(groupOnClickListener)
    }

    /**
     * This method is called for displaying changeable data including recovered data after
     * calling onViewStateRestored() method.
     */
    override fun onResume() {
        super.onResume()
        binding.quantityTextView.text = quantityOfCups.toString()
        displayPricePerCup()
    }

    /**
     * This method saves changeable data in time of changing Activity.
     *
     * @param savedInstanceState a mapping from String keys to various Parcelable values
     */
    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putInt(Constants.QUANTITY_OF_CUPS_KEY, quantityOfCups)
    }

    /**
     * This method recovers changeable data and is called after onActivityCreated()
     * and before onStart(), instantiating them from given Bundle object.
     *
     * @param savedInstanceState a mapping from String keys to various Parcelable values
     */
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null) {
            quantityOfCups = savedInstanceState.getInt(Constants.QUANTITY_OF_CUPS_KEY)
        }
    }

    /**
     * This method adds toppings and puts them to ArrayList .
     */
    private fun selectTopping() {
        if (binding.toppingsWhippedCream.isChecked) toppingNameList.add(getString(ToppingEnum.WHIPPED_CREAM.getName()))
        if (binding.toppingsChocolate.isChecked) toppingNameList.add(getString(ToppingEnum.CHOCOLATE.getName()))
        if (binding.toppingsNutmeg.isChecked) toppingNameList.add(getString(ToppingEnum.NUTMEG.getName()))
        if (binding.toppingsMint.isChecked) toppingNameList.add(getString(ToppingEnum.MINT.getName()))
        if (binding.toppingsIceCream.isChecked) toppingNameList.add(getString(ToppingEnum.ICE_CREAM.getName()))
        if (binding.toppingsPeanutButter.isChecked) toppingNameList.add(getString(ToppingEnum.PEANUT_BUTTER.getName()))
        if (binding.toppingsMarshmallow.isChecked) toppingNameList.add(getString(ToppingEnum.MARSHMALLOW.getName()))
        if (binding.toppingsCardamom.isChecked) toppingNameList.add(getString(ToppingEnum.CARDAMON.getName()))
        if (binding.toppingsHoney.isChecked) toppingNameList.add(getString(ToppingEnum.HONEY.getName()))
        if (binding.toppingsCinnamon.isChecked) toppingNameList.add(getString(ToppingEnum.CINNAMON.getName()))
        if (binding.toppingsSprinkles.isChecked) toppingNameList.add(getString(ToppingEnum.SPRINKLES.getName()))
        if (binding.toppingsNutella.isChecked) toppingNameList.add(getString(ToppingEnum.NUTELLA.getName()))
        if (binding.toppingsLavender.isChecked) toppingNameList.add(getString(ToppingEnum.LAVENDER.getName()))
        if (binding.toppingsTurmeric.isChecked) toppingNameList.add(getString(ToppingEnum.TURMERIC.getName()))
        if (binding.toppingsSweetCream.isChecked) toppingNameList.add(getString(ToppingEnum.SWEET_CREAM.getName()))
    }

    /**
     * This method increases the quantity of cups and makes verifying
     * of maximum quantity.
     */
    private fun increment() {
        if (quantityOfCups < 10) {
            quantityOfCups++
            binding.quantityTextView.text = quantityOfCups.toString()
            MultimediaEffects.playSound(this.requireContext(), R.raw.quantity_buttons)
        }
        if (quantityOfCups == 10) {
            MultimediaEffects.displayToast(
                this.requireContext(),
                getString(R.string.maximum_size_of_order))
        }
    }

    /**
     * This method decreases the quantity of cups and makes verifying
     * of minimum quantity.
     */
    private fun decrement() {
        if (quantityOfCups > 1) {
            quantityOfCups--
            binding.quantityTextView.text = quantityOfCups.toString()
            MultimediaEffects.playSound(this.requireContext(), R.raw.quantity_buttons)
        } else if (quantityOfCups == 1) {
            MultimediaEffects.displayToast(
                this.requireContext(),
                getString(R.string.minimum_size_of_order))
        }
    }

    /**
     * This method displays the given price of one cup on the screen.
     */
    private fun displayPricePerCup() {
        setFragmentResultListener(Constants.FRAGMENT_RESULT_KEY) { _, bundle ->
            coffee = bundle.getParcelable(Constants.COFFEE_OBJECT_KEY)
        }
        binding.pricePerCupTextView.text = "$${coffee?.priceOfCupId}"
        MultimediaEffects.createAnimation(
            this.requireContext(),
            binding.chooseTextView,
            R.anim.translation_from_right_side)
        MultimediaEffects.createAnimation(
            this.requireContext(),
            binding.pricePerCupTextView,
            R.anim.translation_from_right_side)
        MultimediaEffects.createAnimation(
            this.requireContext(),
            binding.perCupTextView,
            R.anim.translation_from_right_side)
    }

    /**
     * This method displays toast message with recovered data of a
     * previous order after calling onRestoreInstanceState() method.
     */
    private fun displayLoadedDataPreferences() {
        val dataPreferences: SharedPreferences =
            activity?.getSharedPreferences(
                Constants.SHARED_PREFERENCES_STORAGE_NAME,
                Context.MODE_PRIVATE)!!
        val previousOrder = dataPreferences.getString(Constants.PREVIOUS_ORDER_KEY, "")
        MultimediaEffects.displayToast(this.requireContext(), previousOrder!!)
    }

    /**
     * Calculates price of the order.
     *
     * @return total price
     */
    private fun calculatePrice(): Int {
        val totalCoffeePrice = quantityOfCups * coffee!!.getPriceOfCupId()
        var totalToppingsPrice = 0
        for (x in toppingNameList) {
            for (y in ToppingEnum.values()) {
                if (x == getString(y.getName())) totalToppingsPrice += quantityOfCups * y.price
            }
        }
        return totalCoffeePrice + totalToppingsPrice
    }

    /**
     * This method creates new EmailFragment & saves instances for it.
     */
    private fun createEmailFragment() {
        val transaction: FragmentTransaction = requireActivity().supportFragmentManager.beginTransaction()
        val bundle: Bundle = bundleOf(
            Constants.QUANTITY_OF_CUPS_KEY to quantityOfCups,
            Constants.TOTAL_PRICE_KEY to calculatePrice(),
            Constants.TOPPINGS_LIST_KEY to toppingNameList,
            Constants.COFFEE_OBJECT_KEY to coffee)
        transaction.add(R.id.fragment_coffee_personal_info_container, EmailFragment::class.java, bundle)
            .addToBackStack(null).commit()
        MultimediaEffects.playSound(this.requireContext(), R.raw.big_buttons)
    }

}