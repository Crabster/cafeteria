package crabster.rudakov.cafeteria.toppingFragment;

import androidx.annotation.StringRes;

import crabster.rudakov.cafeteria.R;

public enum ToppingEnum {

    WHIPPED_CREAM(R.string.whipped_cream, 1),
    CHOCOLATE(R.string.chocolate, 2),
    NUTMEG(R.string.nutmeg, 1),
    MINT(R.string.mint, 1),
    ICE_CREAM(R.string.ice_cream, 2),
    PEANUT_BUTTER(R.string.peanut_butter, 2),
    MARSHMALLOW(R.string.marshmallow, 2),
    CARDAMON(R.string.cardamon, 1),
    HONEY(R.string.honey, 1),
    CINNAMON(R.string.cinnamon, 1),
    SPRINKLES(R.string.sprinkles, 1),
    NUTELLA(R.string.nutella, 2),
    LAVENDER(R.string.lavender, 1),
    TURMERIC(R.string.turmeric, 2),
    SWEET_CREAM(R.string.sweet_cream, 1);

    private final int nameId;
    private final int price;

    ToppingEnum(@StringRes int nameId, int price) {
        this.nameId = nameId;
        this.price = price;
    }

    public int getName() {
        return nameId;
    }

    public int getPrice() {
        return price;
    }

}
