package crabster.rudakov.cafeteria.helpers

import android.content.Context
import android.media.MediaPlayer
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import crabster.rudakov.cafeteria.R

class MultimediaEffects {

    companion object {
        /**
         * This method displays toast message on the screen.
         *
         * @param context application context
         * @param message content of the message
         */
        fun displayToast(context: Context, message: String) {
            val toast: Toast = Toast.makeText(context,
                    message,
                    Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.BOTTOM, 0, 0)
            toast.show()
            playSound(context, R.raw.toast_message)
        }

        /**
         * This method plays sounds.
         *
         * @param context application context
         * @param resId type of a melody from sources
         */
        fun playSound(context: Context, resId: Int) {
            val song: MediaPlayer = MediaPlayer.create(context, resId)
            song.start()
        }

        /**
         * This method creates animation of Views.
         *
         * @param context application context
         * @param view view, that is animated
         * @param resAnimId type of an animation from sources
         */
        fun createAnimation(context: Context, view: View, resAnimId: Int) {
            val animation: Animation = AnimationUtils.loadAnimation(context, resAnimId)
            view.startAnimation(animation)
        }
    }

}