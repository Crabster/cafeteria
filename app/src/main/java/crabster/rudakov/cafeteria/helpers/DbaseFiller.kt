package crabster.rudakov.cafeteria.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import crabster.rudakov.cafeteria.R
import crabster.rudakov.cafeteria.coffeeActivity.coffeDataBase.CoffeeDbManager
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee

class DbaseFiller(val context: Context) {

    private val dbManager: CoffeeDbManager = CoffeeDbManager(context)

    /**
     * This method fills the table of database by different kinds of objects transactionally.
     */
    @SuppressLint("ResourceType")
    fun fillCoffeeDataBase() {
        dbManager.getDb()?.beginTransaction()
        val startTime: Long = System.currentTimeMillis()
        try {
            dbManager.addCoffee(
                Coffee(R.drawable.flag_brazil,
                    context.resources.getString(R.string.country_brazil),
                    R.string.arabica_sort,
                    5,
                    R.string.description_brazilian))

            dbManager.addCoffee(Coffee(R.drawable.flag_colombia,
                    context.resources.getString(R.string.country_colombia),
                    R.string.arabica_sort,
                    5,
                    R.string.description_colombian))

            dbManager.addCoffee(Coffee(R.drawable.flag_ivory_coast,
                    context.resources.getString(R.string.country_ivory_coast),
                    R.string.robusta_sort,
                    5,
                    R.string.description_ivory_coast))

            dbManager.addCoffee(Coffee(R.drawable.flag_angola,
                    context.resources.getString(R.string.country_angola),
                    R.string.robusta_sort,
                    6,
                    R.string.description_angolan))

            dbManager.addCoffee(Coffee(R.drawable.flag_uganda,
                    context.resources.getString(R.string.country_uganda),
                    R.string.robusta_sort,
                    6,
                    R.string.description_ugandan))

            dbManager.addCoffee(Coffee(R.drawable.flag_mexico,
                    context.resources.getString(R.string.country_mexico),
                    R.string.arabica_sort,
                    5,
                    R.string.description_mexican))

            dbManager.addCoffee(Coffee(R.drawable.flag_indonesia,
                    context.resources.getString(R.string.country_indonesia),
                    R.string.robusta_sort,
                    5,
                    R.string.description_indonesian))

            dbManager.addCoffee(Coffee(R.drawable.flag_salvador,
                    context.resources.getString(R.string.country_salvador),
                    R.string.arabica_sort,
                    5,
                    R.string.description_salvadorian))

            dbManager.addCoffee(Coffee(R.drawable.flag_guatemala,
                    context.resources.getString(R.string.country_guatemala),
                    R.string.arabica_sort,
                    7,
                    R.string.description_guatemalan))

            dbManager.addCoffee(Coffee(R.drawable.flag_dr_congo,
                    context.resources.getString(R.string.country_dr_congo),
                    R.string.robusta_sort,
                    8,
                    R.string.description_congolese))

            dbManager.addCoffee(Coffee(R.drawable.flag_madagascar,
                    context.resources.getString(R.string.country_madagascar),
                    R.string.robusta_sort,
                    6,
                    R.string.description_madagascan))

            dbManager.addCoffee(Coffee(R.drawable.flag_costa_rica,
                    context.resources.getString(R.string.country_costa_rica),
                    R.string.arabica_sort,
                    8,
                    R.string.description_costa_rican))

            dbManager.addCoffee(Coffee(R.drawable.flag_venezuela,
                    context.resources.getString(R.string.country_venezuela),
                    R.string.arabica_sort,
                    5,
                    R.string.description_venezuelan))

            dbManager.addCoffee(Coffee(R.drawable.flag_india,
                    context.resources.getString(R.string.country_india),
                    R.string.robusta_sort,
                    5,
                    R.string.description_indian))

            dbManager.addCoffee(Coffee(R.drawable.flag_cameroon,
                    context.resources.getString(R.string.country_cameroon),
                    R.string.robusta_sort,
                    7,
                    R.string.description_cameroonian))

            dbManager.addCoffee(Coffee(R.drawable.flag_peru,
                    context.resources.getString(R.string.country_peru),
                    R.string.arabica_sort,
                    7,
                    R.string.description_peruvian))

            dbManager.addCoffee(Coffee(R.drawable.flag_dominican_republic,
                    context.resources.getString(R.string.country_dominican_republic),
                    R.string.arabica_sort,
                    6,
                    R.string.description_dominican))

            dbManager.addCoffee(Coffee(R.drawable.flag_ecuador,
                    context.resources.getString(R.string.country_ecuador),
                    R.string.arabica_sort,
                    5,
                    R.string.description_ecuadorian))

            dbManager.addCoffee(Coffee(R.drawable.flag_tanzania,
                    context.resources.getString(R.string.country_tanzania),
                    R.string.robusta_sort,
                    8,
                    R.string.description_tanzanian))

            dbManager.addCoffee(Coffee(R.drawable.flag_kenya,
                    context.resources.getString(R.string.country_kenya),
                    R.string.arabica_sort,
                    5,
                    R.string.description_kenyan))

            dbManager.addCoffee(Coffee(R.drawable.flag_cuba,
                    context.resources.getString(R.string.country_cuba),
                    R.string.arabica_sort,
                    5,
                    R.string.description_cuban))

            dbManager.addCoffee(Coffee(R.drawable.flag_ethiopia,
                    context.resources.getString(R.string.country_ethiopia),
                    R.string.arabica_sort,
                    6,
                    R.string.description_ethiopian))

            dbManager.addCoffee(Coffee(R.drawable.flag_honduras,
                    context.resources.getString(R.string.country_honduras),
                    R.string.arabica_sort,
                    7,
                    R.string.description_honduran))

            dbManager.addCoffee(Coffee(R.drawable.flag_paraguay,
                    context.resources.getString(R.string.country_paraguay),
                    R.string.robusta_sort,
                    5,
                    R.string.description_paraguayan))

            dbManager.addCoffee(Coffee(R.drawable.flag_philippines,
                    context.resources.getString(R.string.country_philippines),
                    R.string.robusta_sort,
                    6,
                    R.string.description_filipino))

            dbManager.addCoffee(Coffee(R.drawable.flag_sierra_leone,
                    context.resources.getString(R.string.country_sierra_leone),
                    R.string.robusta_sort,
                    7,
                    R.string.description_sierra_leonean))

            dbManager.addCoffee(Coffee(R.drawable.flag_east_timor,
                    context.resources.getString(R.string.country_east_timor),
                    R.string.robusta_sort,
                    8,
                    R.string.description_east_timorian))

            dbManager.addCoffee(Coffee(R.drawable.flag_yemen,
                    context.resources.getString(R.string.country_yemen),
                    R.string.arabica_sort,
                    8,
                    R.string.description_yemeni))

            dbManager.addCoffee(Coffee(R.drawable.flag_vietnam,
                    context.resources.getString(R.string.country_vietnam),
                    R.string.robusta_sort,
                    5,
                    R.string.description_vietnamese))

            dbManager.addCoffee(Coffee(R.drawable.flag_zimbabwe,
                    context.resources.getString(R.string.country_zimbabwe),
                    R.string.arabica_sort,
                    6,
                    R.string.description_zimbabwean))

            dbManager.addCoffee(Coffee(R.drawable.flag_bolivia,
                    context.resources.getString(R.string.country_bolivia),
                    R.string.arabica_sort,
                    6,
                    R.string.description_bolivian))

            dbManager.addCoffee(Coffee(R.drawable.flag_cambodia,
                    context.resources.getString(R.string.country_cambodia),
                    R.string.robusta_sort,
                    5,
                    R.string.description_cambodian))

            dbManager.addCoffee(Coffee(R.drawable.flag_gabon,
                    context.resources.getString(R.string.country_gabon),
                    R.string.robusta_sort,
                    5,
                    R.string.description_gabonese))

            dbManager.addCoffee(Coffee(R.drawable.flag_ghana,
                    context.resources.getString(R.string.country_ghana),
                    R.string.robusta_sort,
                    5,
                    R.string.description_ghanaian))

            dbManager.addCoffee(Coffee(R.drawable.flag_liberia,
                    context.resources.getString(R.string.country_liberia),
                    R.string.robusta_sort,
                    6,
                    R.string.description_liberian))

            dbManager.addCoffee(Coffee(R.drawable.flag_panama,
                    context.resources.getString(R.string.country_panama),
                    R.string.arabica_sort,
                    8,
                    R.string.description_panamanian))

            dbManager.addCoffee(Coffee(R.drawable.flag_rwanda,
                    context.resources.getString(R.string.country_rwanda),
                    R.string.arabica_sort,
                    7,
                    R.string.description_rwandan))

            dbManager.addCoffee(Coffee(R.drawable.flag_thailand,
                    context.resources.getString(R.string.country_thailand),
                    R.string.robusta_sort,
                    5,
                    R.string.description_thai))

            dbManager.addCoffee(Coffee(R.drawable.flag_togo,
                    context.resources.getString(R.string.country_togo),
                    R.string.robusta_sort,
                    6,
                    R.string.description_togolese))

            dbManager.addCoffee(Coffee(R.drawable.flag_zambia,
                    context.resources.getString(R.string.country_zambia),
                    R.string.arabica_sort,
                    7,
                    R.string.description_zambian))

            dbManager.addCoffee(Coffee(R.drawable.flag_australia,
                    context.resources.getString(R.string.country_australia),
                    R.string.arabica_sort,
                    8,
                    R.string.description_australian))

            dbManager.addCoffee(Coffee(R.drawable.flag_malawi,
                    context.resources.getString(R.string.country_malawi),
                    R.string.arabica_sort,
                    6,
                    R.string.description_malawian))

            dbManager.addCoffee(Coffee(R.drawable.flag_burundi,
                    context.resources.getString(R.string.country_burundi),
                    R.string.arabica_sort,
                    5,
                    R.string.description_burundian))

            dbManager.addCoffee(Coffee(R.drawable.flag_trinidad_and_tobago,
                    context.resources.getString(R.string.country_trinidad_and_tobago),
                    R.string.arabica_sort,
                    8,
                    R.string.description_trinidadian))

            dbManager.addCoffee(Coffee(R.drawable.flag_central_african_republic,
                    context.resources.getString(R.string.country_central_african_republic),
                    R.string.robusta_sort,
                    7,
                    R.string.description_central_african))

            dbManager.addCoffee(Coffee(R.drawable.flag_shri_lanka,
                    context.resources.getString(R.string.country_shri_lanka),
                    R.string.robusta_sort,
                    6,
                    R.string.description_shri_lankan))

            dbManager.addCoffee(Coffee(R.drawable.flag_haiti,
                    context.resources.getString(R.string.country_haiti),
                    R.string.arabica_sort,
                    6,
                    R.string.description_haitian))

            dbManager.addCoffee(Coffee(R.drawable.flag_jamaica,
                    context.resources.getString(R.string.country_jamaica),
                    R.string.arabica_sort,
                    8,
                    R.string.description_jamaican))

            dbManager.addCoffee(Coffee(R.drawable.flag_laos,
                    context.resources.getString(R.string.country_laos),
                    R.string.robusta_sort,
                    5,
                    R.string.description_lao))

            dbManager.addCoffee(Coffee(R.drawable.flag_nigeria,
                    context.resources.getString(R.string.country_nigeria),
                    R.string.robusta_sort,
                    5,
                    R.string.description_nigerian))

            dbManager.getDb()?.setTransactionSuccessful()
        } finally {
            dbManager.getDb()?.endTransaction()
            val endTime: Long = System.currentTimeMillis() - startTime
            Log.d(Constants.FILLING_DATABASE_TIME_KEY, endTime.toString())
        }
    }

}