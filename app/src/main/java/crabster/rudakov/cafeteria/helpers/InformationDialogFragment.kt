package crabster.rudakov.cafeteria.helpers

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import crabster.rudakov.cafeteria.R

class InformationDialogFragment : DialogFragment() {

    /**
     * This method is creates dialog window.
     *
     * @param savedInstanceState a mapping from String keys to various values
     * @return Dialog window
     */
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity, R.style.InformationDialogView)
        return builder.setTitle(getString(R.string.information_title_dialog_window))
                      .setView(layoutInflater.inflate(R.layout.activity_main_dialog, null, false))
                      .setIcon(R.mipmap.ic_information)
                      .setPositiveButton(getString(R.string.ok_button_dialog_window), null)
                      .create()
    }

}