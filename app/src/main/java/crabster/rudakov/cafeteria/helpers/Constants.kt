package crabster.rudakov.cafeteria.helpers

import crabster.rudakov.cafeteria.R

object Constants {

    const val QUANTITY_OF_CUPS_KEY: String = "quantityOfCups"
    const val COFFEE_OBJECT_KEY: String = "coffee"
    const val TOPPINGS_LIST_KEY: String = "toppings"
    const val TOTAL_PRICE_KEY: String = "totalPrice"
    const val FLAG_ID_KEY: String = "zoomableFlag"
    const val MIME_DATA_TYPE_KEY: String = "text/plain"
    const val DATABASE_NAME: String = "coffeeBase.db"
    const val INTENT_FILTER_NAME_ZOOMABLE: String = "android.intent.action.ZOOM"
    const val SHARED_PREFERENCES_STORAGE_NAME: String = "Saved Data"
    const val PREVIOUS_ORDER_KEY: String = "PreviousOrder"
    const val FILLING_DATABASE_TIME_KEY: String = "TIME IT TOOK TO FILLING"
    const val FRAGMENT_RESULT_KEY = "Fragment result API"
    const val URI_GOOGLE_MAPS: String = "https://www.google.com/maps/place/"
    const val URI_GOOGLE_SEARCH: String = "https://www.google.com/search?q="
    const val COFFEE_SEARCH_ADDITIONAL_KEY: Int = R.string.search_additional_key
    const val VERSION: Int = 1

}