package crabster.rudakov.cafeteria.zoomableActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import crabster.rudakov.cafeteria.R
import crabster.rudakov.cafeteria.helpers.Constants

class ZoomableActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoomable)
        val imageView: SubsamplingScaleImageView = findViewById(R.id.zoomable_map_image_view)

        val flagId: Int = intent.getIntExtra(Constants.FLAG_ID_KEY, 0)
        imageView.setImage(ImageSource.resource(flagId))
    }

}