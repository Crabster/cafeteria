package crabster.rudakov.cafeteria.emailFragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import crabster.rudakov.cafeteria.R
import crabster.rudakov.cafeteria.coffeeActivity.recyclerViewHelpers.Coffee
import crabster.rudakov.cafeteria.databinding.FragmentEmailBinding
import crabster.rudakov.cafeteria.helpers.Constants
import crabster.rudakov.cafeteria.helpers.MultimediaEffects.Companion.displayToast
import crabster.rudakov.cafeteria.helpers.MultimediaEffects.Companion.playSound
import java.lang.StringBuilder
import java.util.ArrayList

open class EmailFragment : Fragment() {

    private lateinit var dataValidator: DataValidator
    private val orderString = StringBuilder()
    private lateinit var toppingNameList: ArrayList<String>

    private var _binding: FragmentEmailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEmailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toppingNameList = requireArguments().getStringArrayList(Constants.TOPPINGS_LIST_KEY)!!

        binding.sendEMail.setOnClickListener { verifyEnteredData() }
        binding.cancelSendEMail.setOnClickListener { returnToToppingFragment() }
    }

    /**
     * This method cleans reference to current fragment's layout.
     */
    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }

    /**
     * This method returns to the previous fragment & cleans the list of
     * chosen toppings.
     */
    private fun returnToToppingFragment() {
        playSound(this.requireContext(), R.raw.big_buttons)
        displayToast(this.requireContext(), getString(R.string.choose_again_message))
        toppingNameList.clear()
        parentFragmentManager.popBackStack()
    }

    /**
     * This method verifies user's name & e-mail.
     * In case of successfully verifying it creates e-mail body.
     */
    private fun verifyEnteredData() {
        val enteredName = binding.enterNameView.text.toString()
        val enteredEmail = binding.enterEMailView.text.toString()
        dataValidator = DataValidator(enteredName, enteredEmail)
        if (!dataValidator.validateName() || TextUtils.isEmpty(enteredName)) {
            displayToast(this.requireContext(), getString(R.string.enter_name))
        } else if (!dataValidator.validateEmail() || TextUtils.isEmpty(enteredEmail)) {
            displayToast(this.requireContext(), getString(R.string.enter_e_mail))
        } else {
            playSound(this.requireContext(), R.raw.big_buttons)
            createEmailMessage()
        }
    }

    /**
     * This method sends order to a given e-mail address and makes report about it.
     */
    @SuppressLint("QueryPermissionsNeeded")
    private fun createEmailMessage() {
        val totalPrice: Int = requireArguments().getInt(Constants.TOTAL_PRICE_KEY)
        val intentSend = Intent(Intent.ACTION_SEND)
        intentSend.type = Constants.MIME_DATA_TYPE_KEY
        intentSend.putExtra(Intent.EXTRA_EMAIL, arrayOf(dataValidator.enteredEmail))
        intentSend.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.order_result))
        intentSend.putExtra(Intent.EXTRA_TEXT, createOrderSummary(totalPrice))
        if (intentSend.resolveActivity(activity?.packageManager!!) != null) {
            saveDataPreferences(createDataPreferences())
            startActivity(intentSend)
            binding.emailSendingReport.setText(R.string.email_sending_report)
        }
    }

    /**
     * This method creates text message.
     *
     * @param price total price
     * @return text message, that's put into a body of e-mail
     */
    private fun createOrderSummary(price: Int): String {
        val quantityOfCups: Int = requireArguments().getInt(Constants.QUANTITY_OF_CUPS_KEY)
        val coffee: Coffee = requireArguments().getParcelable(Constants.COFFEE_OBJECT_KEY)!!

        orderString.append(getString(R.string.name) + ": \"" + dataValidator.enteredName + "\"\n\n")
            .append(getString(coffee.getSortId()) + " " + getString(R.string.e_mail_message_from))
            .append(" " + coffee.getCountryOfOriginId() + "\n")
            .append(getString(R.string.quantity_message) + ": \"" + quantityOfCups + "\" * " + coffee.getPriceOfCupId() + "$")

        for (topping in toppingNameList) {
            orderString.append("\n" + getString(R.string.add_topping) + " " + topping + " * " + quantityOfCups)
        }

        orderString.append("\n\n" + getString(R.string.total_price) + ": \"" + price + "$\"")
            .append("\n\n" + getString(R.string.thank_you))
        return orderString.toString()
    }

    /**
     * This method creates text with information about current order, that
     * would be shown at next launch of the application.
     *
     * @return text for toast message
     */
    private fun createDataPreferences(): String {
        orderString.delete(0, orderString.indexOf("\n") + 2)
            .delete(orderString.lastIndexOf("\n") - 2, orderString.length)
            .insert(0, getString(R.string.previous_order_message) + "\n\n")
        return orderString.toString()
    }

    /**
     * This method saves parameters of the order into XML file.
     *
     * @param order parameters of the order
     */
    private fun saveDataPreferences(order: String) {
        val dataPreferences: SharedPreferences =
            activity?.getSharedPreferences(
                Constants.SHARED_PREFERENCES_STORAGE_NAME,
                Context.MODE_PRIVATE)!!
        val editor = dataPreferences.edit()
        editor.putString(Constants.PREVIOUS_ORDER_KEY, order)
        editor.apply()
    }

}