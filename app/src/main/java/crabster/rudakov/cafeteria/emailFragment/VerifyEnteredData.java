package crabster.rudakov.cafeteria.emailFragment;

public interface VerifyEnteredData {

    /**
     * This method validates entered name with regex.
     */
    boolean validateName();

    /**
     * This method validates entered e-mail address with regex.
     */
    boolean validateEmail();

}
