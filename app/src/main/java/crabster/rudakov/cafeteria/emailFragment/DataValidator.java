package crabster.rudakov.cafeteria.emailFragment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataValidator implements VerifyEnteredData {

    private final String enteredName;
    private final String enteredEmail;
    private final Pattern validEmailRegex;
    private final Pattern validNameRegex;

    public DataValidator(String enteredName, String enteredEmail) {
        this.enteredName = enteredName;
        this.enteredEmail = enteredEmail;
        validEmailRegex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
                Pattern.CASE_INSENSITIVE);
        validNameRegex = Pattern.compile("^[\\p{L} .'-]+$", Pattern.CASE_INSENSITIVE);
    }

    public String getEnteredName() {
        return enteredName;
    }

    public String getEnteredEmail() {
        return enteredEmail;
    }

    /**
     * This method validates entered name with regex.
     *
     * @return result of matching with regex
     */
    @Override
    public boolean validateName() {
        Matcher matcher = validNameRegex.matcher(enteredName);
        return matcher.find();
    }

    /**
     * This method validates entered e-mail address with regex.
     *
     * @return result of matching with regex
     */
    @Override
    public boolean validateEmail() {
        Matcher matcher = validEmailRegex.matcher(enteredEmail);
        return matcher.find();
    }

}
