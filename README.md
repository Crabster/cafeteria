# Cafeteria
-----------------------------------------------------------------------------------------------------------------------------------------------------
## Welcome to our coffee shop!

*This application presents you with our entire range and will help you make the right choice.   
Based on the presented assortment, you can choose any type of coffee you like, as well as add any toppings to it.   
The cost of your order will be calculated and sent by email.   
The application is localized to work in three languages.   
We hope that you will be satisfied!*
-----------------------------------------------------------------------------------------------------------------------------------------------------
### Languages:

* Java + Kotlin 

### Applied technologies:

* RecyclerView 
* Fragments 
* ViewBinding 
* SQLite 

### External libraries & plugins:
* Subsampling Scale Image View 
* Kotlin Parcelize plugin 
-----------------------------------------------------------------------------------------------------------------------------------------------------
![image](app/src/main/res/drawable-v24/coffee_activity.jpg)